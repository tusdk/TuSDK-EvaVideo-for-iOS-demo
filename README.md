# 视频融合文档 - iOS

### 库与资源依赖

#### 库依赖

- TuSDK.framework
- TuSDKPulse.framework
- libyuv.framework
- TuSDKPulseEva.framework

#### 系统库依赖
- libresolv.tbd
- libiconv.tbd
- libz.tbd
- libc++.tbd


#### 资源依赖

- TuSDK.bundle文件 (权限认证在这里，需要开通对应的[权限]([https://tutucloud.com](https://tutucloud.com/))，导出拥有使用权限的包)



### 初始化

- 头文件引入

需要在使用功能的类中导入`#import "TuSDKFramework.h"`, 此文件可以从Demo中拿到，或者直接自己写一个引用文件，进行依赖

- 初始化SDK

在引用`#import "TuSDKFramework.h"`的`.m`文件中，需要将后缀改成`.mm`
在`AppDelegate.mm`中的`didFinishLaunchingWithOptions`方法中初始化SDK

```objective-c
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [TuSDK initSdkWithAppKey:@"8d0ad6cca31401a7-04-ewdjn1"];
    // 可选: 设置日志输出级别 (默认不输出)
    [TuSDK setLogLevel:lsqLogLevelDEBUG];

    return YES;
}
```

- 初始化Engine
需要在首页导入`#import "TuSDKPulse/TUPEngine.h"`

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化
    [TUPEngine Init:nil];
}
```


### 使用

#### 模板加载

模板加载使用的是`TUPDisplayView`和`TUPEvaModel`类。

需要在使用功能的类中导入`#import <TuSDKPulse/TUPDisplayView.h>、#import <TuSDKPulseEva/TUPEvaPlayer.h>、#import <TuSDKPulseEva/TUPEvaDirector.h>`


- 模板加载(根据 模板路径 初始化)

```objective-c
- (instancetype) init:(NSString*)path;
//获取模板内的文字资源
- (NSArray<TextReplaceItem*>*) listReplaceableTextAssets;
//获取模板内的图片资源
- (NSArray<VideoReplaceItem*>*) listReplaceableImageAssets;
//获取模板内的视频资源
- (NSArray<VideoReplaceItem*>*) listReplaceableVideoAssets;
//获取模板内的音频资源
- (NSArray<AudioReplaceItem*>*) listReplaceableAudioAssets;

```


```
    self.displayView = [[TUPDisplayView alloc] init];
    [self.preview addSubview:self.displayView];
    
    [self.displayView setup:nil];

    //根据文件路径初始化model
    TUPEvaModel *model = [[TUPEvaModel alloc]init:self.evaPath];
    //初始化eva
    self.evaDirector = [[TUPEvaDirector alloc] init];
    [self.evaDirector openModel:model];

    //获取eva播放器
    self.evaPlayer = (TUPEvaDirectorPlayer *)[self.evaDirector newPlayer];
    [self.evaPlayer open];
    self.evaPlayer.delegate = self;

    [self.displayView attachPlayer:weakSelf.evaPlayer];
    //开始播放
    [self.evaPlayer play];
```

-播放器状态api
```objective-c
- (void) close;

- (BOOL) play;

- (BOOL) pause;
```

- 播放器代理回调`<TUPPlayerDelegate>`

```
- (void)onPlayerEvent:(TUPPlayerState)state withTimestamp:(NSInteger)ts
{
    self.playerState = state;
}
```

- 资源替换

```
- (BOOL) updateText:(NSString*)Id withText:(NSString*) text;

- (BOOL) updateImage:(NSString*)Id withPath:(NSString*) path andConfig:(TUPEvaReplaceConfig_ImageOrVideo*)config;

- (BOOL) updateVideo:(NSString*)Id withPath:(NSString*) path andConfig:(TUPEvaReplaceConfig_ImageOrVideo*)config;

- (BOOL) updateAudio:(NSString*)Id withPath:(NSString*) path andConfig:(TUPEvaReplaceConfig_Audio*)config;
```

示例
```
//替换音乐
    if (self.selectedPath && self.selectPathState  == TuSelectFilePathMusicState) {
        TUPEvaReplaceConfig_Audio *audioConfig = [[TUPEvaReplaceConfig_Audio alloc] init];
        audioConfig.start = 0;
        audioConfig.duration = [self.evaPlayer getDuration];
        audioConfig.audioMixWeight = self.volmSlider.value;
        
        AudioReplaceItem *item = self.audioAssets.firstObject;
        item.resPath = self.selectedPath;
        [self.evaDirector updateAudio:item.Id withPath:self.selectedPath andConfig:audioConfig];
    }
    //替换图片
    if (self.selectedPath && self.selectPathState == TuSelectFilePathImageState) {
        //选中的图片资源
        VideoReplaceItem *asset = _orgResources[_index];
        TUPEvaReplaceConfig_ImageOrVideo *imageConfig = [[TUPEvaReplaceConfig_ImageOrVideo alloc] init];
        imageConfig.start = asset.startTime;
        imageConfig.duration = asset.endTime - asset.startTime;
        imageConfig.audioMixWeight = self.volmSlider.value;

        [self.evaDirector updateImage:asset.Id withPath:self.selectedPath andConfig:imageConfig];
        
    }
    //替换视频
    if (self.selectedPath && self.selectPathState == TuSelectFilePathVideoState) {
        //选中的视频资源,根据isVideo字段判断相对应涂层
        VideoReplaceItem *asset = _orgResources[_index];

        if (asset.isVideo) {

            [self.evaDirector updateVideo:asset.Id withPath:self.selectedPath andConfig:self.videoConfig];
        } else {

            [self.evaDirector updateImage:asset.Id withPath:self.selectedPath andConfig:self.videoConfig];
            
        }
    }
```

- 资源导出器
```
    TUPEvaModel *model = [[TUPEvaModel alloc]init:self.evaPath];
    //初始化eva
    self.evaDirector = [[TUPEvaDirector alloc] init];
    [self.evaDirector openModel:model];

    TUPProducer_OutputConfig *config = [[TUPProducer_OutputConfig alloc] init];
    //开始时间
    config.rangeStart = 0;
    //时长
    config.rangeDuration = self.totalTime;
    //水印
    config.watermark = [UIImage imageNamed:@"sample_watermark"];
    //水印位置
    config.watermarkPosition = -1;
    
    self.producer = (TUPEvaDirectorProducer *)[self.evaDirector newProducer];
    self.producer.delegate = self;
    self.producer.savePath = [@"file://" stringByAppendingString:savePath];
    
    [self.producer setOutputConfig:config];
    [self.producer open];
    [self.producer start];
```

- 播放器代理回调`<TUPProducerDelegate>`

```
- (void) onProducerEvent:(TUPProducerState)state withTimestamp:(NSInteger) ts
{
    self.playerState = state;
}
```



### 其他

- 使用前需要先获取权限，拿到与bundle ID 一致的授权Key和TuSDK.bundle资源
- Demo中的zip资源包不可用于商用，有版权约束的
- zip资源包的生成请看对应的文档
- 其他更详细的API请看SDK中头文件中的API
- 如果报`memory not found`，将引入`TuSDKFramework.h`或直接引入了`#import <TuSDKEva/TuSDKEva.h>`的文件后缀改成`.mm`即可


